<?php
/*
Plugin Name: Ubicador Tamex
Description: Servicio de goelocalización para pedido de tamales en la página de Tamalexpress, desarrollo hecho a la medida.
Author: Pablo César Sánchez Porta
Version: 1.0.0
 */

/**
 * Menú en Administrador
 */
function tmx_menu(){
	add_menu_page('mapa',
				'Mapa pedidos',
				'administrator',
				'tmx-settings',
				'tmx_page_settings',
				'dashicons-location-alt');
}
add_action('admin_menu', 'tmx_menu');
/**
 * Hoja de estilos para página de ajustes
 */
function tmx_style_settings(){
	wp_register_style('tmx_admin_css', plugin_dir_url(__FILE__) . 'css/admin.css', false, '1.0.0');
	wp_enqueue_style('tmx_admin_css');
}
add_action('admin_enqueue_scripts', 'tmx_style_settings');
/**
 * Página de ajustes
 */
function tmx_page_settings(){
	add_action('admin_footer', 'tmx_admin_gmaps_api', 20);
	ob_start();
	include( plugin_dir_path(__FILE__) . 'includes/admin_form.php');
	$out = ob_get_contents();
	ob_end_clean();
	echo $out; 
}
/**
 * Google Maps JavaScript API en admin
 */
function tmx_admin_gmaps_api(){
	if(get_option('tmx_gmaps_api_key')!=''){
		ob_start();
		include( plugin_dir_path(__FILE__) . 'js/admin_js.php' );
		$out = ob_get_contents();
		ob_end_clean();
		$out .= '<script async defer src="https://maps.googleapis.com/maps/api/js?key='.get_option('tmx_gmaps_api_key').'&callback=initMap"></script>';
		echo $out;
	}
}
/**
 * Ajustes
 */
function tmx_settings(){
	register_setting('tmx-settings-group',
		'tmx_gmaps_api_key');
	register_setting('tmx-settings-group',
		'tmx_cobertura');
	register_setting('tmx-settings-group',
		'tmx_cobertura_strokeColor');
	register_setting('tmx-settings-group',
		'tmx_cobertura_fillColor');
	register_setting('tmx-settings-group',
		'tmx_cobertura_zoom');
	register_setting('tmx-settings-group',
		'tmx_cobertura_mensaje');
	register_setting('tmx-settings-group',
		'tmx_cobertura_url');
	register_setting('tmx-settings-group',
		'tmx_cobertura_method');
	register_setting('tmx-settings-group',
		'tmx_cobertura_token');
	register_setting('tmx-settings-group',
		'tmx_cobertura_mensajes');
}
add_action('admin_init', 'tmx_settings');

/**
 * Google Maps JavaScript API en frontend
 */
function tmx_gmaps_api(){
	if(get_option('tmx_gmaps_api_key')){
		ob_start();
		include( plugin_dir_path(__FILE__) . 'js/tmx_js.php' );
		$out = ob_get_contents();
		ob_end_clean();
		$out .= '<script async defer src="https://maps.googleapis.com/maps/api/js?key='.get_option('tmx_gmaps_api_key').'&callback=initMap"></script>';
		echo $out;
	}
}
/**
 * Hoja de estilos para mapa en frontend
 */
function tmx_style(){
	wp_register_style('modalcss', plugin_dir_url(__FILE__) . 'css/modal.css', '', '', 'all');
	wp_register_script('modaljs', plugin_dir_url(__FILE__) . 'js/bootstrap.min.js', array('jquery'), '1', true);
	wp_register_style('tmx_css', plugin_dir_url(__FILE__) . 'css/tmx.css', false, '1.0.0');
	wp_enqueue_script('modaljs');
	wp_enqueue_style('modalcss');
	wp_enqueue_style('tmx_css');
}
	add_action('wp_enqueue_scripts', 'tmx_style');

/**
 * Shortcode
 * @return Mapa de ubicacion par seleccionar el punto de entrega
 */
function tmx_map(){
	add_filter('wp_footer', 'tmx_gmaps_api');
	ob_start();
	include( plugin_dir_path(__FILE__) . 'includes/mapa.php' );
	$out = ob_get_contents();
	ob_end_clean();
	return $out;
}
add_shortcode('tmx_mapa', 'tmx_map');