<?php
/**
 * Vista de mapa para muestra de shortcode
 * @author Pablo césar Sánchez Porta <pcsp85@gmail.com>
 * @package tmx_ubicacion\includes\mapa
 */
$mensajes = get_option('tmx_cobertura_mensajes');
?>
<div id="select">
	<?php echo $mensajes['intro']; ?>
	<div class="text-center">
		<button class="button mi_ubicacion" id="get_map">Mi ubicación</button>
		<button class="button otra_ubicacion" id="get_form">Otra dirección</button>
	</div>
</div>
<div id="mapa">
	<div id="cobertura_tamex"></div>
	<p class="text-center"><?php echo $mensajes['indicacion'];?></p>
	<div class="text-center">
		<button class="button continuar" id="map_next">Continuar</button>
	</div>
	
</div>
<div id="form">
	<h3></h3>
	<form action="<?php echo get_option('tmx_cobertura_url');?>" method="<?php echo get_option('tmx_cobertura_method');?>">
		<input type="hidden" name="token" value="<?php echo get_option('tmx_cobertura_token');?>">
		<div class="form-group">
			<label>Código Postal:</label>
			<input type="text" name="CPTxt" id="CPTxt" autocomplete="off" required>
		</div>
		<div class="form-group">
			<label>Calle</label>
			<input type="text" name="CalleTxt" id="CalleTxt" autocomplete="off" required>
		</div>
		<div class="form-group">
			<label>Número Exterior</label>
			<input type="text" name="ExteriorTxt" id="ExteriorTxt" autocomplete="off">
		</div>
		<div class="form-group">
			<label>Número interior</label>
			<input type="text" name="InteriorTxt" id="InteriorTxt" autocomplete="off">
		</div>
		<div class="form-group">
			<label>Colonia</label>
			<input type="text" name="ColoniaTxt" id="ColoniaTxt" autocomplete="off">
		</div>
		<div class="form-group">
			<label>ciudad</label>
			<input type="text" name="CiudadTxt" id="CiudadTxt" autocomplete="off">
		</div>
		<div class="form-group">
			<label>Municipio/Delegación</label>
			<input type="text" name="MunicipioTxt" id="MunicipioTxt" autocomplete="off">
		</div>
		<div class="form-group">
			<label>Estado</label>
			<input type="text" name="EstadoTxt" id="EstadoTxt" autocomplete="off">
		</div>
		<div class="form-group">
			<label>Referencia</label>
			<input type="text" name="ReferenciaTxt" id="ReferenciaTxt" autocomplete="off">
		</div>
		<div class="form-group">
			<label>Nombre</label>
			<input type="text" name="NombreTxt" id="NombreTxt" autocomplete="off" required>
		</div>
		<div class="form-group">
			<label>Apellido Paterno</label>
			<input type="text" name="PaternoTxt" id="PaternoTxt" autocomplete="off">
		</div>
		<div class="form-group">
			<label>Apellido Materno</label>
			<input type="text" name="MaternoTxt" id="MaternoTxt" autocomplete="off">
		</div>
		<div class="form-group">
			<label>Email</label>
			<input type="text" name="EmailTxt" id="EmailTxt" autocomplete="off" required>
		</div>
		<div class="form-group">
			<label>Teléfono</label>
			<input type="text" name="TelefonoTxt" id="TelefonoTxt" autocomplete="off" required>
		</div>
		<div class="clear">
			<br>
		</div>
		<div class="text-center">
			<button class="button cancel">Cancelar</button>
			<button class="button pedido">Comensar pedido</button>
		</div>
	</form>
</div>
<div class="clear"></div>
<div id="mensaje" class="modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" data-dismiss="modal">&times;</button><br>
			</div>
			<div class="modal-body">
				<p></p>
			</div>
		</div>
	</div>
</div>