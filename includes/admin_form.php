<?php
/**
 * Formulario para página de ajustes
 * @author Pablo césar Sánchez Porta <pcsp85@gmail.com>
 * @package tmx_ubicacion\includes\admin_form
 */
?>
<div class="wrap"><h2>Configuración del Mapa Pedidos Tamex</h2>
	<form method="post" action="options.php">
		<?php
			settings_fields('tmx-settings-group');
			do_settings_sections('tmx-settings-group');
		?>
		<div class="form-control">
			<label class="" for="tmx_gmaps_api_key">Clave de API</label>
			<input type="text" name="tmx_gmaps_api_key" id="tmx_gmaps_api_key" value="<?php echo get_option('tmx_gmaps_api_key');?>">
		</div>
		<?php if(get_option('tmx_gmaps_api_key')!=''): ?>
			<div class="cobertura">
				<h3>Zona de cobertura</h3>
				<p>Después de configurar tu zona de cobertura, incluye el "shortcode" en la página que quieras mostrarlo <code>[tmx_mapa]</code>, en caso de que no configures una zona de cobertura, los clientes podrán solicitar el pedido desde cualquier ubicación del mapa.</p>
				<div class="cov_map">
					<div id="tmx_set_cov"></div>
				</div>
				<div class="list">
					<div class="cobertura_config">
						<h4>Configuracón del mapa</h4>
						<p>
							<button id="drop_zona" class="button button-secondary">Olvidar zona de cobertura</button>
						</p>
						<input type="hidden" name="tmx_cobertura" id="tmx_cobertura" value="<?php echo get_option('tmx_cobertura');?>">
						<input type="hidden" name="tmx_cobertura_zoom" id="tmx_cobertura_zoom" value="<?php echo get_option('tmx_cobertura_zoom');?>">
						<div class="form-group">
							<label for="tmx_cobertura_strokeColor">Color de Linea: #</label>
							<input type="text" name="tmx_cobertura_strokeColor" id="tmx_cobertura_strokeColor" placeholder="00CC00" value="<?php echo get_option('tmx_cobertura_strokeColor');?>">
						</div>
						<div class="form-group">
							<label for="tmx_cobertura_fillColor">Color de Fondo: #</label>
							<input type="text" name="tmx_cobertura_fillColor" id="tmx_cobertura_fillColor" placeholder="00CC00" value="<?php echo get_option('tmx_cobertura_fillColor');?>">
						</div>
					</div>
					<div class="covertura_param">
						<h4>Envió de parámetros</h4>
						<div class="form-group">
							<label>URL</label><br>
							<input type="text" name="tmx_cobertura_url" id="tmx_cobertura_url" value="<?php echo get_option('tmx_cobertura_url');?>" required>
						</div>
						<div class="form-group">
							<label>Método de envío de variables</label>
							<select name="tmx_cobertura_method" id="tmx_cobertura_method" required>
								<option value="">Selecciona una opción</option>
								<option value="get">Get</option>
								<option value="post">Post</option>
							</select>
						</div>
						<div class="form-group">
							<label>Token</label><br>
							<input type="text" name="tmx_cobertura_token" id="tmx_cobertura_token" value="<?php echo get_option('tmx_cobertura_token');?>">
						</div>
					</div>
					<p>El plugin envía el resultado de la georeferenciación al destino guardado en el campo URL, según el método seleccionado; los nombres de las variables corresponden al formulario de http://lasmotosdelostamales.com/forma.aspx, si se configura enviará el valor asignado en el campo token tambien como una variable del mismo nombre.</p>
					<p>Es posible editar la hoja de estilos para configrar los botones del plugin, <a href="<?php echo admin_url('plugin-editor.php?file=tmx_ubicacion/css/tmx.css&plugin=tmx_ubicacion/tmx_ubicacion.php');?>">da clic aquí</a></p>
				</div>
				<div class="clear mensajes"><br>
					<h3>Configuración de mensajes</h3>
					<?php $mensajes = get_option('tmx_cobertura_mensajes'); ?>
					<div class="form-group">
						<h4>Menjase para entrada:</h4>
						<?php wp_editor($mensajes['intro'], 'tmx_cobertura_mensajes_intro', array('media_buttons'=>true, 'editor_height'=>200)); ?>

						<h4>Mensaje de confirmación ubicación exitosa (modal):</h4>
						<?php wp_editor($mensajes['exito_ubicacion'], 'tmx_cobertura_mensajes_exito_ubicacion', array('media_buttons'=>true, 'editor_height'=> 200)); ?>
					</div>
					<div class="form-group">
						<h4>Indicación para arrastrar el mapa:</h4><br>
						<input type="text" name="tmx_cobertura_mensajes[indicacion]" id="tmx_cobertura_mensajes_indicacion" value="<?php echo $mensajes['indicacion'];?>">
					</div>
					<div class="form-group">
						<h4 class="error">Título formulario para confirmar dirección:</h4><br>
						<input type="text" name="tmx_cobertura_mensajes[confirmar_ubicacion]" id="tmx_cobertura_mensajes_confirmar_ubicacion" value="<?php echo $mensajes['confirmar_ubicacion'];?>">
					</div>
					<div class="form-group">
						<h4 class="error">Titulo formulario si selecciona "otra ubicación":</h4><br>
						<input type="text" name="tmx_cobertura_mensajes[otra_ubicacion]" id="tmx_cobertura_mensajes_otra_ubicacion" value="<?php echo $mensajes['otra_ubicacion'];?>">
					</div>
					<div class="form-group">
						<h4 class="error">Error si el cliente selecciona su ubicación fuera de la zona cobertura (modal):</h4><br>
						<?php wp_editor($mensajes['no_cobertura'], 'tmx_cobertura_mensajes_no_cobertura', array('media_buttons'=>true, 'editor_height'=> 200)); ?>
					</div>
				</div>
			</div>
		<?php else: ?>
			<div class="error">Debes proporcionar la clave de API para configurar la zona de cobertura</div>
			<p>Este plugin requiere una Clave de API para la utilizar los servicios de Goole Maps JavaScript API.</p>
			<p>Para saber como crear la clave, <a href="https://developers.google.com/maps/documentation/javascript/get-api-key?hl=es" target="_blank"> da clic aquí</a>
			<p>Para crear tu clave, <a href="https://console.developers.google.com/apis/credentials" target="_blank">da clic aquí</a></p>
		<?php endif; ?>
		<div class="clear"></div>
		<?php submit_button();?>
	</form>
</div>