<?php
/**
 * Constructor Script para shortcode  
 * Package: Plugin Ubicador Tamex
 * @author Pablo César Sánchez Porta <pcsp85@gmail.com>
 */
$strokeColor = get_option('tmx_cobertura_strokeColor') == '' ? '00CC00' : get_option('tmx_cobertura_strokeColor');
$fillColor = get_option('tmx_cobertura_fillColor') == '' ? '00CC00' : get_option('tmx_cobertura_fillColor');
$zoom = get_option('tmx_cobertura_zoom') == '' ? '14' : get_option('tmx_cobertura_zoom');
$mensajes = get_option('tmx_cobertura_mensajes');
?>
<script type="text/javascript">
$=jQuery;
var map, center;
var cobertura = [<?php echo get_option('tmx_cobertura');?>];
function initMap() {
	$('#get_map').click(function(){
		$('#select').fadeOut("fast", function(){
			$('#mapa').fadeIn("fast");
			var myLocation = navigator.geolocation.getCurrentPosition(function(position){
				center = {lat:position.coords.latitude, lng:position.coords.longitude}
			  	map = new google.maps.Map(document.getElementById('cobertura_tamex'), {
				    center: center,
				    zoom: <?php echo $zoom;?>
			  	});
			  	mk_center = new google.maps.Marker({
			  		position: center
			  	});
			  	mk_center.setMap(map);
			  	map.addListener('center_changed', function(e){
			  		mk_center.setPosition(map.center);
			  	});
			  	if(cobertura.length>3){
				  	var zona = new google.maps.Polygon({
				  		paths: cobertura,
				  		strokeColor: '#<?php echo $strokeColor;?>',
						strokeOpacity: 0.8,
						fillColor: '#<?php echo $fillColor;?>',
						fillOpacity: 0.3
				  	});
				  	zona.setMap(map);
				  	$('#map_next').click(function(){
				  		if(google.maps.geometry.poly.containsLocation(map.center, zona)){
				  			//Calcular georeferenciación inversa
				  			var geocoder = new google.maps.Geocoder;
				  			geocoder.geocode({location:map.center}, function(result, status){
				  				if(status === google.maps.GeocoderStatus.OK){
				  					console.log(result);
				  					var dir = result[0].address_components;
				  					if(result[0] && result[1]){
				  						var detalle = result.length <= 9 ? result.length <= 7 ? 1 : result.length-6 : 2;
				  						console.log(detalle);
				  						$('#CPTxt').val(dir[dir.length-1].long_name);
				  						$('#CalleTxt').val(dir[1].long_name);
				  						$('#ExteriorTxt').val(dir[0].long_name);
				  						$('#ColoniaTxt').val(result[detalle].address_components[0].long_name);
				  						$('#CiudadTxt').val(result[detalle].address_components[1].long_name).attr('readonly', true);
				  						$('#MunicipioTxt').val(result[detalle].address_components[2].long_name).attr('readonly', true);
				  						$('#EstadoTxt').val(dir[dir.length-3].long_name).attr('readonly',true);
				  						
				  						mensaje('<?php echo $mensajes[exito_ubicacion];?> <p class="text-center"><button class="button Continuar" data-dismiss="modal">Continuar</button></p>');

				  						$('#mapa').fadeOut("fast", function(){
				  							$('#form h3').html('<?php echo $mensajes['confirmar_ubicacion']; ?>');
				  							$("#form").fadeIn("fast");	
				  						});
				  					}else{
				  						mensaje('Ocurrio un problema, no se obtubieron resultados.');
				  					}
				  				}else{
				  					mensaje('Error en el componente de geolocalización: ' + status);
				  				}
				  			});
				  		}else{
				  			//modal indicando que no hay cobertura
				  			mensaje('<?php echo $mensajes['no_cobertura'];?>');
				  		}
				  	});
			  	}else{
			  		$('#map_next').click(function(){
						//Calcular georeferenciación inversa
			  			var geocoder = new google.maps.Geocoder;
			  			geocoder.geocode({location:map.center}, function(result, status){
			  				if(status === google.maps.GeocoderStatus.OK){
			  					console.log(result);
			  					var dir = result[0].address_components;
			  					if(result[0] && result[1]){
			  						var detalle = result.length <= 9 ? result.length <= 7 ? 1 : result.length-6 : 2;
			  						console.log(detalle);
			  						$('#CPTxt').val(dir[dir.length-1].long_name);
			  						$('#CalleTxt').val(dir[1].long_name);
			  						$('#ExteriorTxt').val(dir[0].long_name);
			  						$('#ColoniaTxt').val(result[detalle].address_components[0].long_name);
			  						$('#CiudadTxt').val(result[detalle].address_components[1].long_name).attr('readonly', true);
			  						$('#MunicipioTxt').val(result[detalle].address_components[2].long_name).attr('readonly', true);
			  						$('#EstadoTxt').val(dir[dir.length-3].long_name).attr('readonly',true);
			  						
			  						mensaje('<?php echo $mensajes[exito_ubicacion];?> <p class="text-center"><button class="button Continuar" data-dismiss="modal">Continuar</button></p>');

			  						$('#mapa').fadeOut("fast", function(){
			  							$('#form h3').html('<?php echo $mensajes['confirmar_ubicacion']; ?>');
			  							$("#form").fadeIn("fast");	
			  						});
			  					}else{
			  						mensaje('Ocurrio un problema, no se obtubieron resultados.');
			  					}
			  				}else{
			  					mensaje('Error en el componente de geolocalización: ' + status);
			  				}
			  			});
			  		});
			  	}
			});
		});
	});
	$('#get_form').click(function(){
		$('#select').fadeOut("fast", function(){
			$('#form h3').html('<?php echo $mensajes['otra_ubicacion'] ?>');
			$('#form').fadeIn("fast");
		});
	});
}
var mensaje = function (msj){
	$('#mensaje .modal-body p').html(msj);
	$('#mensaje').modal("show");
};

$('button.cancel').click(function(e){
	e.preventDefault();
	window.location.reload(true);
});
</script>