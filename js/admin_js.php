<?php
/**
 * Constructor Script para Backend 
 * Package: Plugin Ubicador Tamex
 * @author Pablo César Sánchez Porta <pcsp85@gmail.com>
 */
$strokeColor = get_option('tmx_cobertura_strokeColor') == '' ? '00CC00' : get_option('tmx_cobertura_strokeColor');
$fillColor = get_option('tmx_cobertura_fillColor') == '' ? '00CC00' : get_option('tmx_cobertura_fillColor');
$zoom = get_option('tmx_cobertura_zoom') == '' ? '14' : get_option('tmx_cobertura_zoom');
?>
<script type="text/javascript">
var map, center;
var cobertura=[<?php echo get_option('tmx_cobertura'); ?>];
function initMap() {
	var lats=0, lngs=0;
	jQuery(cobertura).each(function(i,e){
		lats+=e.lat;
		lngs+=e.lng;
		if(i==cobertura.length-1){
			center = {lat:(lats/cobertura.length), lng:(lngs/cobertura.length)}
		}
	});
	if(!center) center = {lat:19.43123459272899, lng:-99.13684844970703};
	var myLocation = navigator.geolocation.getCurrentPosition(function(position){
	  	map = new google.maps.Map(document.getElementById('tmx_set_cov'), {
		    center: center,
		    zoom: <?php echo $zoom;?>
	  	});
	  	var zona = new google.maps.Polygon({
	  		paths: cobertura,
	  		strokeColor: '#<?php echo $strokeColor;?>',
			strokeOpacity: 0.8,
			fillColor: '#<?php echo $fillColor;?>',
			fillOpacity: 0.3
	  	});
	  	zona.setMap(map);
		google.maps.event.addListener(map, 'click', function(e){
			addMarker(e.latLng,map);
			cobertura.push({lat:e.latLng.lat(), lng:e.latLng.lng()});
			if(jQuery('input#tmx_cobertura').val()!='') jQuery('input#tmx_cobertura').val( jQuery('input#tmx_cobertura').val()+ ',');
			jQuery('input#tmx_cobertura').val(jQuery('input#tmx_cobertura').val() + "{lat:" +e.latLng.lat()+", lng:"+e.latLng.lng()+"}");
			zona.setMap(null);
			zona = new google.maps.Polygon({
		  		paths: cobertura,
		  		strokeColor: '#<?php echo $strokeColor;?>',
				strokeOpacity: 0.8,
				fillColor: '#<?php echo $fillColor;?>',
				fillOpacity: 0.3
		  	});
			zona.setMap(map);
		});
		jQuery('#drop_zona').click(function(e){
			e.preventDefault();
			jQuery('input#tmx_cobertura').val('');
			cobertura=[];
			zona.setMap(null);
		});
		map.addListener('zoom_changed', function(e){
			jQuery('#tmx_cobertura_zoom').val(map.zoom);
		});
	});

}
function addMarker(location, map) {
  var marker = new google.maps.Marker({
    position: location,
    map: map
  });
}
jQuery(document).ready(function(){
	jQuery('#tmx_cobertura_method').val('<?php echo get_option('tmx_cobertura_method');?>');
	jQuery('#tmx_cobertura_mensajes_intro').attr('name', 'tmx_cobertura_mensajes[intro]');
	jQuery('#tmx_cobertura_mensajes_exito_ubicacion').attr('name', 'tmx_cobertura_mensajes[exito_ubicacion]');
	jQuery('#tmx_cobertura_mensajes_no_cobertura').attr('name', 'tmx_cobertura_mensajes[no_cobertura]');
});
</script>